;(function ( window, document, undefined ) {
    'use strict';

    var map;
    var marker;
    var markers = [];

    var btnStart = document.querySelector('#btnStart');
    var btnReset = document.querySelector('#btnReset');
    var isMapping = false;

    var directionDisplay = new google.maps.DirectionsRenderer();
    var directionService = new google.maps.DirectionsService();

    var indicator = document.querySelector('.indicator');

    var App = {

        init: function () {
            App.isMappingToggler();
            App.initMap();
            App.markerReseter();
        },

        initMap: function () {
            var mapContainer = document.querySelector('.map');

            var mapOpts = {
                center: { lat: -6.108559, lng: 106.779213},
                zoom: 16,
                disableDefaultUI: true,
                styles: [
                    {
                        featureType: 'poi',
                        stylers: [
                            { visibility: 'off' }
                        ]
                    }
                ]
            };

            map = new google.maps.Map( mapContainer, mapOpts );

            directionDisplay.setMap(map);

            google.maps.event.addListener(map, 'click', App.createMarker);
        },

        isMappingToggler: function () {
            btnStart.addEventListener('click', function () {
                var currentState = this.getAttribute('data-is-mapping');

                if ( currentState === 'false' ) {
                    this.textContent = 'End';
                    this.setAttribute('data-is-mapping', 'true');
                    isMapping = true;
                } else if ( currentState === 'true' ) {
                    this.textContent = 'Start';
                    this.setAttribute('data-is-mapping', 'false');
                    isMapping = false;

                    App.createRoute();
                    App.toggleBtnReset();
                    App.toggleBtnStart();
                }
            }, false);
        },

        createRoute: function () {
            App.showIndicator('Getting route');

            var origin = getLatLngFromMarker( markers[0] );
            var destination = getLatLngFromMarker( markers[ markers.length - 1 ] );

            var req = {
                origin: origin,
                destination: destination,
                travelMode: google.maps.TravelMode.DRIVING,
                waypoints: App.getRouteWayPoints()
            };

            directionService.route(req, function ( response, status ) {
                if ( status == google.maps.DirectionsStatus.OK ) {
                    directionDisplay.setDirections( response );
                    directionDisplay.setMap(map);
                    App.disableMarkerDrag();
                    App.dismissIndicator('Route added');
                }
            });
        },

        getRouteWayPoints: function () {
            var routes = [];
            var pointsToReturn = [];

            for ( var i = 0; i < markers.length; i++ ) {
                routes.push( markers[i] );
            }

            routes.splice(0,1);
            routes.pop();

            for ( var j = 0; j < routes.length; j++ ) {
                pointsToReturn.push({
                    location: getLatLngFromMarker( routes[j] ),
                    stopover: false
                });
            }

            return pointsToReturn;
        },

        createMarker: function ( evt ) {
            if ( !isMapping ) return;

            var lat = evt.latLng.lat();
            var lng = evt.latLng.lng();

            App.createMarkerFromPoint( lat, lng );
        },

        createMarkerFromPoint: function ( lat, lng ) {
            var pos = new google.maps.LatLng( lat, lng );

            marker = new google.maps.Marker({
                position: pos,
                map: map,
                draggable: true
            });

            google.maps.event.addListener(marker, 'click', App.killYourSelf);

            markers.push( marker );
        },

        killYourSelf: function ( e ) {
            if ( !isMapping ) return;

            var index = markers.indexOf(this);
            this.setMap(null);
            markers.splice(index, 1);
        },

        markerReseter: function () {
            btnReset.addEventListener('click', function () {
                if ( !markers.length ) return;

                for (var i = markers.length - 1; i >= 0; i--) {
                    markers[i].setMap(null);
                    markers.pop();
                }

                directionDisplay.setMap(null);
                App.toggleBtnReset();
                App.toggleBtnStart();
            }, false);
        },

        addYouAreHereMarker: function ( userPosition ) {
            var userLat = userPosition.coords.latitude;
            var userLng = userPosition.coords.longitude;

            var customIcon = {
                url: 'img/marker-user.gif',
                scaledSize: new google.maps.Size(30, 30)
            };

            var userMarker = new google.maps.Marker({
                position: { lat: userLat, lng: userLng},
                map: map,
                icon: customIcon,
                optimized: false
            });
            
            map.panTo({
                lat: userLat,
                lng: userLng
            });

            App.dismissIndicator('I have eyes on you');
        },

        disableMarkerDrag: function () {
            for (var i = markers.length - 1; i >= 0; i--) {
                markers[i].setDraggable(false);
            }
        },

        toggleBtnReset: function () {
            btnReset.classList.toggle('sr-only');
        },

        toggleBtnStart: function () {
            if ( !btnStart.hasAttribute('disabled') ) {
                btnStart.setAttribute('disabled', 'disabled');
            } else {
                btnStart.removeAttribute('disabled');
            }
        },

        showIndicator: function ( msg ) {
            indicator.classList.remove('indicator--success', 'indicator--hidden');
            indicator.innerHTML = msg;
        },

        dismissIndicator: function ( msg ) {
            indicator.classList.add('indicator--success');
            indicator.innerHTML = msg;

            setTimeout( function () {
                indicator.classList.add('indicator--hidden');
            }, 2000);
        }

    };

    if ( 'geolocation' in navigator ) {
        navigator.geolocation.getCurrentPosition( function (position) {
            App.addYouAreHereMarker( position );
        });
    }

    App.init();

    function getLatLngFromMarker( marker ) {
        var lat = marker.position.lat();
        var lng = marker.position.lng();

        return new google.maps.LatLng( lat, lng );
    }

})( window, document );